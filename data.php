<?php

class Data { 
    public $aMemberVar ; 
    public $aFuncName = 'aMemberFunc'; 
    
    
    function aMemberFunc() { 
     	$room_type = ['Single','Double','Executive','Luxury','type a','type b','type c'];
     	$room_type_json = json_encode($room_type);
     
     	$days_in_month = ['01', '02', '03', '04', '05', '06', '07','08', '09', '10','11','12','13', '14', '15', '16', '17', '18','19', '20', '21', '22', '23', '24','25','26','27','28','29','30'];
		$days_in_month_json = json_encode($days_in_month);
		
		$date_postion_occurance = array();
		for ($i=0; $i < 29; $i++) { 
			$date_postion_occurance[$i]= [rand(0,6),rand(0,29),rand(0,15)];
		}
		$date_postion_occurance_json =json_encode($date_postion_occurance); 
		
	   //===========================  pulling js  =================== 
        
        $jscript_to_inject = array();
		  
     	$jscript_to_inject[] = "<script type='text/javascript' src='assets/js/echarts-all-3.js'></script>";
	    $jscript_to_inject[] = "<script type='text/javascript' src='assets/js/ecStat.min.js'></script>";
		$jscript_to_inject[] = "<script type='text/javascript' src='assets/js/assets/js/dataTool.min.js'></script>";
		$jscript_to_inject[] = "<script type='text/javascript' src='assets/js/china.js'></script>";
		$jscript_to_inject[] = "<script type='text/javascript' src='assets/js/world.js'></script>";
		$jscript_to_inject[] = "<script type='text/javascript' src='http://api.map.baidu.com/api?v=2.0&ak=ZUONbpqGBsYGXNIYHicvbAbM'></script>";
		$jscript_to_inject[] = "<script type='text/javascript' src='assets/js/bmap.min.js'></script>";


        $jscript_to_inject[] = "<script type='text/javascript'>";

		$jscript_to_inject[] =	"var dom = document.getElementById('container');";
		$jscript_to_inject[] =	"var myChart = echarts.init(dom);";
		$jscript_to_inject[] =	"var app = {};";
		$jscript_to_inject[] =	"option = null;";
		$jscript_to_inject[] =	"app.title = 'Pluio';";
		$jscript_to_inject[] =	"var hours = $days_in_month_json";
		$jscript_to_inject[] =	"var days = $room_type_json";

		$jscript_to_inject[] =	"var data = $date_postion_occurance_json";
		$jscript_to_inject[] =	"option = {";
		$jscript_to_inject[] =	    "tooltip: {";
		$jscript_to_inject[] =	        "position: 'top'";
		$jscript_to_inject[] =	    "},";
		$jscript_to_inject[] =	   " title: [],";
		$jscript_to_inject[] =	    "singleAxis: [],";
		$jscript_to_inject[] =	    "series: []";
		$jscript_to_inject[] =	"};";

		$jscript_to_inject[] =	"echarts.util.each(days, function (day, idx) {";
		$jscript_to_inject[] =	    "option.title.push({";
		$jscript_to_inject[] =	        "textBaseline: 'middle',";
		$jscript_to_inject[] =	      "  top: (idx + 0.5) * 100 / 7 + '%',";
		$jscript_to_inject[] =	      "  text: day";
		$jscript_to_inject[] =	    "});";
		$jscript_to_inject[] =	    "option.singleAxis.push({";
		$jscript_to_inject[] =	        "left: 150,";
		$jscript_to_inject[] =	        "type: 'category',";
		$jscript_to_inject[] =	       " boundaryGap: false,";
		$jscript_to_inject[] =        "data: hours,";
		$jscript_to_inject[] =        "top: (idx * 100 / 7 + 5) + '%',";
		$jscript_to_inject[] =        "height: (100 / 7 - 10) + '%',";
		$jscript_to_inject[] =       " axisLabel: {";
		$jscript_to_inject[] =           " interval: 2";
		$jscript_to_inject[] =	        "}";
		$jscript_to_inject[] =	    "});";
		$jscript_to_inject[] =	    "option.series.push({";
		$jscript_to_inject[] =	        "singleAxisIndex: idx,";
		$jscript_to_inject[] =	        "coordinateSystem: 'singleAxis',";
		$jscript_to_inject[] =	        "type: 'scatter',";
		$jscript_to_inject[] =	       " data: [],";
		$jscript_to_inject[] =	        "symbolSize: function (dataItem) {";
		$jscript_to_inject[] =	            "return dataItem[1] * 4;";
		$jscript_to_inject[] =	        "}";
		$jscript_to_inject[] =	    "});";
		$jscript_to_inject[] =	"});";

		$jscript_to_inject[] =	"echarts.util.each(data, function (dataItem) {";
		$jscript_to_inject[] =	    "option.series[dataItem[0]].data.push([dataItem[1], dataItem[2]]);";
		$jscript_to_inject[] =	"});;";
		$jscript_to_inject[] =	"if (option && typeof option === 'object') {";
		$jscript_to_inject[] =	    "myChart.setOption(option, true);";
		$jscript_to_inject[] =	"}";
			        
		$jscript_to_inject[]=	 "</script>";
			       
			         // 
		 return implode("\n", $jscript_to_inject);
    } 
} 

$data = new Data; 
$var =  $data->aMemberFunc();
echo $var;

 ?>